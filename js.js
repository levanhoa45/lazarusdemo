
var faq = document.getElementsByClassName("faq-question");
var i;
for (i = 0; i < faq.length; i++) {
    faq[i].addEventListener("click", function () {
        this.classList.toggle("active");
        var body = this.nextElementSibling;
        if (body.style.display === "block") {
            body.style.display = "none";
        } else {
            body.style.display = "block";
        }
    });
}

$(function() {
  
  $('#dropzone input').on('change', function(e) {
    var file = this.files[0];
    
    if ((/^image\/(gif|png|jpeg)$/i).test(file.type)) {
      var reader = new FileReader(file);

      reader.readAsDataURL(file);
      
      reader.onload = function(e) {
        var data = e.target.result,
            $img = $('<img/>').attr('src', data).fadeIn();
        
        $('#dropzone div').html($img);
      };
    } else {
      var ext = file.name.split('.').pop();
      
      $('#dropzone div').html(ext);
    }
  });
});
var slider = document.getElementById("slider");
var selector = document.getElementById("selector");
var SelectValue = document.getElementById("SelectValue");
var ProgressBar = document.getElementById("ProgressBar");
SelectValue.innerHTML = slider.value;

slider.oninput = function(){
    SelectValue.innerHTML = this.value;
    selector.style.left = this.value + "0%";
    ProgressBar.style.width = this.value + "0%";
}

function ReadMoreFunction() {
    var dots = document.getElementById("dots");
    var moreText = document.getElementById("more");
    var ReadMoreText = document.getElementById("ReadMore");
  
    if (dots.style.display === "none") {
      dots.style.display = "inline";
      ReadMoreText.innerHTML = "More FAQ's"; 
      moreText.style.display = "none";
    } else {
      dots.style.display = "none";
      ReadMoreText.innerHTML = "Read Less"; 
      moreText.style.display = "inline";
    }
} 
window.onscroll = function() {scrollFunction()};

function scrollFunction(){
  if(($('html').scrollTop())>=(($('.banner').offset()).top)-800)
  {
      $('.banner').addClass('path');
  }
  else
  {
      $('.banner').removeClass('path');
  };
  //works
  if(($('html').scrollTop())>=(($('.works').offset()).top)-700)
  {
      $('.works').addClass('path1');
      $('.works').addClass('path2');
      $('.works').addClass('path3');
  }
  else
  {
      $('.works').removeClass('path1');
      $('.works').removeClass('path2');
      $('.works').removeClass('path3');
  };
  //plan
  if(($('html').scrollTop())>=(($('.pricing-plans').offset()).top)-1200)
  {
      $('.pricing-plans').addClass('path4');
  }
  else
  {
      $('.pricing-plans').removeClass('path4');
  };
  //mission
  if(($('html').scrollTop())>=(($('.mission').offset()).top)-750)
  {
      $('.mission').addClass('path5');
  }
  else
  {
      $('.mission').removeClass('path5');
  };
}
$('#lazarus1').click(function()
{
    $('html').animate({scrollTop:0},1000);
    return false;			
});
$('#works1').click(function()
{
    $('html').animate({scrollTop:($('.works').offset().top)-100},1000);
    return false;			
});
$('#pricing1').click(function()
{
    $('html').animate({scrollTop:($('.pricing-plans').offset().top)-120},1000);
    return false;			
});
$(document).ready(function (){
  $('.slider').slick({
    slidesToShow: 1,
    slidesToScroll: 1,
    dots: true,
    infinite: true,
    autoplay: true,
    autoplaySpeed: 4000,
    prevArrow:"<button type='button' class='slick-prev slick-arrow'><i class='fa fa-arrow-left' aria-hidden='true'></i></button>",
    nextArrow:"<button type='button' class='slick-next slick-arrow'><i class='fa fa-arrow-right' aria-hidden='true'></i></button>",
    customPaging: function (slider, i) {
      var title = $(slider.$slides[i]).data('title');
      return '<span class=""><span class="dots__number">0'+ Number(i+1) + '</span>' + title + ' </span>';
    },
    dotsClass: 'slider-dots'
  });

});